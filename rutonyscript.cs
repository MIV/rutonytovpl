using System;
using System.Threading;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using WebSocketSend;
     
namespace RutonyChat {
	
    public class Script {

        public void InitParams(string param) {
			
			string strtosend = "{\"init\" : true, \"user\" : \"StreamerName\", \"text\" : \"Текст\"}";
			ProgramWS.sendtoVKPlayLive(strtosend);
			
            RutonyBot.SayToWindow("Автономный VKPlay-скрипт подключен v1.0");
			RutonyBot.SayToWindow("В браузерное расширение отправлена команда запуска. Если страница с чатом не открылась - значит случилас какая-то фигня.");
        }

        public void Closing() {
            RutonyBot.SayToWindow("Автономный VKPlay ТУК-скрипт отключен v1.0");
        }

        public void NewMessage(string site, string name, string text, bool system) {
            
			//Получаем профиль чатланина на случай операций с балансом
            RankControl.ChatterRank cr = RankControl.ListChatters.Find(r => r.Nickname == name.Trim().ToLower());
				
				if (site == "vkplay") {
    				string ansString = "Вижу твоё сообщение, " + name;
                    string strtosend = "{\"user\" : \"StreamerName\", \"text\" : \" " + ansString  + " \"}";
					ProgramWS.sendtoVKPlayLive(strtosend);
					
				}
        }

    }
}

namespace WebSocketSend
{
    class ProgramWS
    {
        public static async Task sendtoVKPlayLive(string text)
        {
            var client = new ClientWebSocket();
            await client.ConnectAsync(new Uri("ws://127.0.0.1:8484"), CancellationToken.None);
            await FnsendtoVKPlayLive(client, text);
            await client.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing the connection", CancellationToken.None);
        }

        static async Task FnsendtoVKPlayLive(ClientWebSocket client, string text)
        {
			var buffer = Encoding.UTF8.GetBytes(text);
            await client.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Text, true, CancellationToken.None);
        }
    }
}
