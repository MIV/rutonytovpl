//{"init" : true, "user" : "StreamerName", "text" : "Текст"}
const STREAMER = "StreamerName";
const URL = `https://vkplay.live/${STREAMER}/only-chat`;
const SOCKET_URL = '127.0.0.1:8484';

let messagetochat;

// Connect to the socket
const socket = new WebSocket(`ws://${SOCKET_URL}`);
socket.onmessage = (event) => {
	const message = JSON.parse(decodeURIComponent(event.data));
	
	if (message.hasOwnProperty('user') && message.user == STREAMER) {
        console.log(`Данные получены с сервера: ${decodeURIComponent(event.data)}`);        

        if (message.hasOwnProperty('text') || message.hasOwnProperty('init')) {
            messagetochat = message.text;

            chrome.tabs.query({
                url: URL
            }, (tabs) => {
                if (tabs.length === 0) {
                    chrome.tabs.create({
                        url: URL
                    }, (tab) => {
                        if (!message.hasOwnProperty('init')) {
                            chrome.tabs.sendMessage(tab.id, {
                                type: 'gettoken'
                            });
                        }
                    });
                } else {
                    if (!message.hasOwnProperty('init')) {
                        chrome.tabs.sendMessage(tabs[0].id, {
                            type: 'gettoken'
                        });
                    }
                }
            });
        }
    }
};

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request.type === 'gottoken') {

        var myHeaders = new Headers();
        myHeaders.append("Accept", "application/json, text/plain, */*");
        myHeaders.append("Authorization", `Bearer ${request.token}`);
        myHeaders.append("Cache-Control", "no-cache");
        myHeaders.append("Connection", "keep-alive");
        myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
        myHeaders.append("Origin", "https://vkplay.live");
        myHeaders.append("Pragma", "no-cache");
        myHeaders.append("Referer", `https://vkplay.live/${STREAMER}/only-chat`);
        myHeaders.append("Sec-Fetch-Dest", "empty");
        myHeaders.append("Sec-Fetch-Mode", "cors");
        myHeaders.append("Sec-Fetch-Site", "same-site");
        myHeaders.append("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 YaBrowser/23.1.1.1138 Yowser/2.5 Safari/537.36");
        myHeaders.append("X-App", "streams_web");
        myHeaders.append("sec-ch-ua", "\"Not?A_Brand\";v=\"8\", \"Chromium\";v=\"108\", \"Yandex\";v=\"23\"");
        myHeaders.append("sec-ch-ua-mobile", "?0");
        myHeaders.append("sec-ch-ua-platform", "\"Windows\"");

        var urlencoded = new URLSearchParams();
        urlencoded.append("data", `[{\"type\":\"text\",\"content\":\"[\\\"${messagetochat}\\\",\\\"unstyled\\\",[]]\",\"modificator\":\"\"},{\"type\":\"text\",\"content\":\"\",\"modificator\":\"BLOCK_END\"}]`);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded,
            redirect: 'follow'
        };

        fetch(`https://api.vkplay.live/v1/blog/${STREAMER}/public_video_stream/chat`, requestOptions)
            .then(response => response.text())
            .then(result => console.log(result))
            .catch(error => console.log('error', error));

    }
});
