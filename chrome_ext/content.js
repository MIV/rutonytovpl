chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
	console.log('Alert here ' + request.type);
  if (request.type === 'gettoken') {
	accessToken = JSON.parse(localStorage.getItem('auth')).accessToken;
	sendResponse(accessToken);
	chrome.runtime.sendMessage({ type: 'gottoken', token: accessToken });
  }
});
