const WebSocket = require('ws');

const server = new WebSocket.Server({
  port: 8484,
  host: '0.0.0.0'
});

server.on('connection', (socket) => {
  console.log('Client connected');

  socket.on('message', (message) => {
    console.log(`Received message: ${message}`);
	let data = JSON.parse(message);
    
    server.clients.forEach((client) => {
      if (client !== socket && client.readyState === WebSocket.OPEN) {
		client.send(JSON.stringify(data));
      }
    });
  });

  socket.on('close', () => {
    console.log('Client disconnected');
  });
});
